/*                                                                             
 * Copyright(c) 2017 Communication Technologies Co., Ltd              
 * All rights reserved.                                                        
 *                                                                             
 * Company : Shanghai Research Institute of China Telecom Corporation
 * Department : IOT Terminal
 * Project : AEP
 * Title  : transport.c                                                      
 * Date: 2018.01.02                                                         
 * Author:  Terminal Group                                                          
 * Revision:  1.1.1                                                              
 */  
 
#include "transport.h"
#include "esp8266.h"
#include "stm32l4xx_hal.h"
#include "uart.h"
#include "string.h"
#include "main.h"

extern UART_HandleTypeDef huart1;
extern char USART_RX_BUF[Uart_Buf_Max];
uint16_t getdata_flag = 0;

/*******************************************************************
* Name: transport_sendPacketBuffer								   
* Input param: data, len
* Return-Value: 0
* Description: send message
*******************************************************************/
int transport_sendPacketBuffer(unsigned char* data,int len)
{
	HAL_UART_Transmit(&huart1, (unsigned char*)data, len, 1000);
	return 0;
}

/*******************************************************************
* Name: transport_getdata								   
* Input param: 
* Return-Value: 
* Description: get message from receive buf
*******************************************************************/
int transport_getdata(unsigned char* buf, int count)
{
	int rc;
	int i;
	for (i = 0; i < count; i++ )
	{
		buf[i] = USART_RX_BUF[getdata_flag];
		getdata_flag++;
	}
	if (getdata_flag == Uart_Buf_Max)
		getdata_flag = 0;
	rc = count;
	return rc;
}

/*******************************************************************
* Name: transport_open								   
* Input param: 
* Return-Value: 
* Description: create TCP/IP socket by wifi mode esp8266 
*******************************************************************/
int transport_open()
{

	esp8266_station();
	esp8266_connect();
	esp8266_set_singleway();
	esp8266_tcp_socket();
	esp8266_set_dtu(); 
    esp8266_set_send();	
	return 0;
}

/*******************************************************************
* Name: transport_close								   
* Input param: 
* Return-Value: 
* Description: close TCP/IP socket
*******************************************************************/
int transport_close()
{
	esp8266_set_sendquit();
    esp8266_close_tcp();
	return 0;
}
