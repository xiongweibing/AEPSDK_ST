#ifndef _UART_H_
#define _UART_H_

#define Uart_Buf_Max 	512
#define Uart_OK			1 
#define Uart_ERROR		2 

void CLR_Buf(void);
unsigned int Send_Command(char * Command, char *Response, unsigned long Timeout, unsigned char Retry);
#endif

